package com.eric.erwin.BudgetWebApp.dao.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BUDGET")
public class Budget {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="BUDGET_ID")
	private Long budgetId;
	
	@Column(name ="ENTITY_ITEM")
	private String budgetItem;
	
	@Column(name = "ENTITY_VALUE")
	private Double budgetItemValue;
	
	@Column(name = "DUE_DATE")
	private Date dueDate;
	
	@Column(name = "PAID_DATE")
	private Date paidDate;
	
	@Column(name = "PAID_AMOUNT")
	private Double paidAmount;

	public Long getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(Long budgetId) {
		this.budgetId = budgetId;
	}

	public String getBudgetItem() {
		return budgetItem;
	}

	public void setBudgetItem(String budgetItem) {
		this.budgetItem = budgetItem;
	}

	public Double getBudgetItemValue() {
		return budgetItemValue;
	}

	public void setBudgetItemValue(Double budgetItemValue) {
		this.budgetItemValue = budgetItemValue;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getPaidDate() {
		return paidDate;
	}

	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}

	public Double getPaidAmount() {
		return paidAmount;
	}

	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}

	@Override
	public String toString() {
		return "Budget [budgetId=" + budgetId + ", budgetItem=" + budgetItem + ", budgetItemValue=" + budgetItemValue
				+ ", dueDate=" + dueDate + ", paidDate=" + paidDate + ", paidAmount=" + paidAmount + "]";
	}
}
