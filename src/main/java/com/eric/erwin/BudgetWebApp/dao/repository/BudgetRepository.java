package com.eric.erwin.BudgetWebApp.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.eric.erwin.BudgetWebApp.dao.model.Budget;

@Transactional
public interface BudgetRepository extends JpaRepository <Budget, Long> {

}
