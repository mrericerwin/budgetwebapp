package com.eric.erwin.BudgetWebApp.validator;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.eric.erwin.BudgetWebApp.model.BudgetDTO;

@Service
public class BudgetValidator implements Validator {

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return BudgetDTO.class.isAssignableFrom(arg0);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		BudgetDTO budgetDto = (BudgetDTO) target;
		
		if(budgetDto.getBudgetItem() == null || budgetDto.getBudgetItem().trim().isEmpty()) {
			errors.reject(Integer.toString(HttpStatus.BAD_REQUEST.value()),"Budget Item Cannot Be Blank Or Empty" );
		} else if (budgetDto.getBudgetItemValue() == null || budgetDto.getBudgetItemValue() < 0.0) {
			errors.reject(Integer.toString(HttpStatus.BAD_REQUEST.value()),"Budget Value Cannot Be Blank Or Less Than Zero!" );
		} else if(budgetDto.getDueDate() == null) {
			errors.reject(Integer.toString(HttpStatus.BAD_REQUEST.value()),"Budget Due Date Cannot Be Blank!" );
		}
	}
}
