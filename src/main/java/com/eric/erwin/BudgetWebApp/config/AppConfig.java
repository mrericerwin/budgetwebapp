package com.eric.erwin.BudgetWebApp.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

import com.eric.erwin.BudgetWebApp.mapper.ResourceMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Configuration
@EnableTransactionManagement	
public class AppConfig {
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "budget.datasource")
	public  DataSourceProperties budgetDataSourceProperties() {
        return new DataSourceProperties();
    }
	
	@Primary
	@Bean
	@ConfigurationProperties(prefix = "budget.datasource")
	public HikariDataSource dataSource(DataSourceProperties properties) {
        return properties.initializeDataSourceBuilder().type(HikariDataSource.class)
                .build();
    }
	
	
	@Bean
	public Jackson2ObjectMapperBuilder jacksonBuilder() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.indentOutput(true).dateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		return builder;
	}
	
	@Bean
	MapperFacade mapperFacade() {
		MapperFactory factory = new DefaultMapperFactory.Builder().build();
		return factory.getMapperFacade();
	}
	
	@Bean
	public ResourceMapper resourceMapper() {
		return new ResourceMapper();
	}
	
	@Bean
	public RestTemplate budgetRestTemplate() {
		RestTemplate template = new RestTemplate();
		
		ObjectMapper objectMapper = jacksonBuilder().build();
		objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
		
		MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
		jsonMessageConverter.setObjectMapper(objectMapper);
		
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(jsonMessageConverter);
		
		template.setMessageConverters(messageConverters);
		
		return template;
	}

}
