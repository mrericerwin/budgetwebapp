package com.eric.erwin.BudgetWebApp.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eric.erwin.BudgetWebApp.dao.model.Budget;
import com.eric.erwin.BudgetWebApp.dao.repository.BudgetRepository;
import com.eric.erwin.BudgetWebApp.mapper.ResourceMapper;
import com.eric.erwin.BudgetWebApp.model.BudgetDTO;

@Service
public class BudgetService {
	
private static final Logger LOGGER = LoggerFactory.getLogger(BudgetService.class);
	
	@Autowired
	private ResourceMapper resourceMapper; 
	
	@Autowired
	private BudgetRepository budgetRepository;
	
	/**
	 * USED FOR SAVE AND/OR UPDATE BUDGET ITEMS
	 * @param budgetItems
	 * @param action
	 * @return
	 * @throws Exception
	 */
	public Boolean saveBudgetItems(List<BudgetDTO> budgetDtoList, String action) throws Exception {
		LOGGER.info( action + "Budget Items...");
		try {
		List<Budget> budgetDaoList = resourceMapper.convertBudgetDtoListToBudgetDaoList(budgetDtoList);
		
		budgetRepository.saveAll(budgetDaoList);
		
		return true;
		} catch (Exception e) {
			LOGGER.error("Issue {} Budget Items. Logging Exception: " + e.toString(), action);
			LOGGER.error("Full Trace: ", e);
			throw new Exception("Issue with " + action + " Budget Items. Error" + e.toString());
		}
	}


	/**
	 * USER TO DELETE BUDGET ITEMS
	 * @param budgetItems
	 * @return
	 * @throws Exception
	 */
	public Boolean deleteBudgetItems(List<BudgetDTO> budgetDtoList) throws Exception {
		LOGGER.info("Deleting Budget Items...");
		try {
			
			List<Budget> budgetDaoList = resourceMapper.convertBudgetDtoListToBudgetDaoList(budgetDtoList);
			
			budgetRepository.deleteAll(budgetDaoList);
			
			return true; 
			
		} catch (Exception e) {
			LOGGER.error("Issue Deleting Budget Items. Logging Exception: " + e.toString());
			LOGGER.error("Full Trace: ", e);
			throw new Exception("Issue with Deleting Budget Items. Error" + e.toString());
		}
	}


	/**
	 * USED TO RETRIEVE BUDGET ITEMS
	 * @return
	 * @throws Exception 
	 */
	public List<BudgetDTO> getBudgetItems() throws Exception {
		
		try {
		LOGGER.info("Retrieving Budget Items...");
		
		List<Budget> budgetDaoList = budgetRepository.findAll();
		
		List<BudgetDTO> budgetDtoList = resourceMapper.convertBudgetDaoListToBudgetDtoList(budgetDaoList);
		
		LOGGER.info("Returning {} Budget Items.", budgetDtoList.size());
		
		return budgetDtoList;
		
		} catch (Exception e) {
			LOGGER.error("Issue Retrieving Budget Items. Logging Exception: " + e.toString());
			LOGGER.error("Full Trace: ", e);
			throw new Exception("Issue with Retrieving Budget Items. Error" + e.toString());
		}
	}

}
