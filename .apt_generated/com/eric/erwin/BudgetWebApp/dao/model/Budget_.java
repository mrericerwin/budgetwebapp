package com.eric.erwin.BudgetWebApp.dao.model;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Budget.class)
public abstract class Budget_ {

	public static volatile SingularAttribute<Budget, String> budgetItem;
	public static volatile SingularAttribute<Budget, Date> paidDate;
	public static volatile SingularAttribute<Budget, Date> dueDate;
	public static volatile SingularAttribute<Budget, Double> budgetItemValue;
	public static volatile SingularAttribute<Budget, Long> budgetId;
	public static volatile SingularAttribute<Budget, Double> paidAmount;

	public static final String BUDGET_ITEM = "budgetItem";
	public static final String PAID_DATE = "paidDate";
	public static final String DUE_DATE = "dueDate";
	public static final String BUDGET_ITEM_VALUE = "budgetItemValue";
	public static final String BUDGET_ID = "budgetId";
	public static final String PAID_AMOUNT = "paidAmount";

}

