package com.eric.erwin.BudgetWebApp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.eric.erwin.BudgetWebApp.dao.model.Budget;
import com.eric.erwin.BudgetWebApp.model.BudgetDTO;

import ma.glasnost.orika.MapperFacade;

public class ResourceMapper {
	@Autowired
	private MapperFacade mapper;
	
	public List<Budget> convertBudgetDtoListToBudgetDaoList(List<BudgetDTO> budgetDtoList) {
		List<Budget> budgetDaoList = new ArrayList<>();
		
		budgetDtoList.forEach(budgetDto -> budgetDaoList.add(mapper.map(budgetDto, Budget.class)));
		
		return budgetDaoList;
	}

	public List<BudgetDTO> convertBudgetDaoListToBudgetDtoList(List<Budget> budgetDaoList) {
		List<BudgetDTO> budgetDtoList = new ArrayList<>();
		
		budgetDaoList.forEach(budgetDao -> budgetDtoList.add(mapper.map(budgetDao, BudgetDTO.class)));
		
		return budgetDtoList;
	}
}
