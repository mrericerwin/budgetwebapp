package com.eric.erwin.BudgetWebApp.model;

import java.io.Serializable;
import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class BudgetDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6594127770042001126L;
	private Long budgetId; 
	private String budgetItem;
	private Double budgetItemValue;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dueDate;
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date paidDate;
	private Double paidAmount;
	public Long getBudgetId() {
		return budgetId;
	}
	public void setBudgetId(Long budgetId) {
		this.budgetId = budgetId;
	}
	public String getBudgetItem() {
		return budgetItem;
	}
	public void setBudgetItem(String budgetItem) {
		this.budgetItem = budgetItem;
	}
	public Double getBudgetItemValue() {
		return budgetItemValue;
	}
	public void setBudgetItemValue(Double budgetItemValue) {
		this.budgetItemValue = budgetItemValue;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Date getPaidDate() {
		return paidDate;
	}
	public void setPaidDate(Date paidDate) {
		this.paidDate = paidDate;
	}
	public Double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(Double paidAmount) {
		this.paidAmount = paidAmount;
	}
	@Override
	public String toString() {
		return "BudgetDTO [budgetId=" + budgetId + ", budgetItem=" + budgetItem + ", budgetItemValue=" + budgetItemValue
				+ ", dueDate=" + dueDate + ", paidDate=" + paidDate + ", paidAmount=" + paidAmount + "]";
	}
}
