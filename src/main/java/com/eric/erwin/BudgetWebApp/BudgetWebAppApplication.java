package com.eric.erwin.BudgetWebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.eric.erwin")
public class BudgetWebAppApplication {
	public static void main(String[] args) {
		SpringApplication.run(BudgetWebAppApplication.class, args);
	}
}
