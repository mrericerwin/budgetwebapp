package com.eric.erwin.BudgetWebApp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.eric.erwin.BudgetWebApp.model.BudgetDTO;
import com.eric.erwin.BudgetWebApp.service.BudgetService;
import com.eric.erwin.BudgetWebApp.validator.BudgetValidator;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/budget")
public class BudgetController {
private static final Logger LOGGER = LoggerFactory.getLogger(BudgetController.class);
	
	
	@Autowired
	private BudgetValidator budgetValidator;
	
	@Autowired
	private BudgetService budgetService; 
	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
	@ApiImplicitParam(name = "USER_ID", required = true, dataType = "string", paramType = "header")
	@ApiOperation(value = "Save List of Budget Items",
				notes = "Saves a List of Budget Items that the user has created.",
				response = Boolean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Invalid Request"),
			@ApiResponse(code = 401, message = "Unathorized"),
			@ApiResponse(code = 404, message = "Data Not Found"),
			@ApiResponse(code = 500, message = "Internal Service Error")
	})
	public Boolean saveBudgetItems(@RequestBody List<BudgetDTO> budgetItems, BindingResult errors) throws Exception {
		
		budgetItems.forEach(budgetItem -> ValidationUtils.invokeValidator(budgetValidator, budgetItem, errors));
		
		if(errors.hasErrors()) {
			throw new BindException(errors);
		}
		
		LOGGER.info("Budget Saving Request Accepted. Processing {} records ", budgetItems.size());
		
		
		return budgetService.saveBudgetItems(budgetItems, "SAVING"); 
		
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.PATCH, produces = "application/json")
	@ApiImplicitParam(name = "USER_ID", required = true, dataType = "string", paramType = "header")
	@ApiOperation(value = "Save List of Budget Items",
				notes = "Saves a List of Budget Items that the user has created.",
				response = Boolean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Invalid Request"),
			@ApiResponse(code = 401, message = "Unathorized"),
			@ApiResponse(code = 404, message = "Data Not Found"),
			@ApiResponse(code = 500, message = "Internal Service Error")
	})
	public Boolean updateBudgetItems(@RequestBody List<BudgetDTO> budgetItems, BindingResult errors) throws Exception {
		
		budgetItems.forEach(budgetItem -> ValidationUtils.invokeValidator(budgetValidator, budgetItem, errors));
		
		if(errors.hasErrors()) {
			throw new BindException(errors);
		}
		
		LOGGER.info("Budget Update Request Accepted. Processing {} records ", budgetItems.size());
		
		
		return budgetService.saveBudgetItems(budgetItems, "UPDATING"); 
		
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE, produces = "application/json")
	@ApiImplicitParam(name = "USER_ID", required = true, dataType = "string", paramType = "header")
	@ApiOperation(value = "Save List of Budget Items",
				notes = "Saves a List of Budget Items that the user has created.",
				response = Boolean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Invalid Request"),
			@ApiResponse(code = 401, message = "Unathorized"),
			@ApiResponse(code = 404, message = "Data Not Found"),
			@ApiResponse(code = 500, message = "Internal Service Error")
	})
	public Boolean deleteBudgetItems(@RequestBody List<BudgetDTO> budgetItems) throws Exception {
		
		Boolean valid = true;
		
		for(BudgetDTO budgetDto : budgetItems) {
			if(budgetDto.getBudgetId() == null || budgetDto.getBudgetId() <= 0L) {
				valid = false;
			}
		}
		
		if(!valid) {
			throw new Exception("One or more Budget Items are not valid!");
		}
		
		LOGGER.info("Budget Delete Request Accepted. Processing {} records ", budgetItems.size());
		
		
		return budgetService.deleteBudgetItems(budgetItems); 
		
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET, produces = "application/json")
	@ApiImplicitParam(name = "USER_ID", required = true, dataType = "string", paramType = "header")
	@ApiOperation(value = "Save List of Budget Items",
				notes = "Saves a List of Budget Items that the user has created.",
				response = Boolean.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success"),
			@ApiResponse(code = 400, message = "Invalid Request"),
			@ApiResponse(code = 401, message = "Unathorized"),
			@ApiResponse(code = 404, message = "Data Not Found"),
			@ApiResponse(code = 500, message = "Internal Service Error")
	})
	public List<BudgetDTO>getBudgetItems() throws Exception {
		
		LOGGER.info("Retrieving Budget Items...");
		
		
		return budgetService.getBudgetItems();
		
	}
}
