package com.eric.erwin.BudgetWebApp.config;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * SWAGGER 2 CONFIGURATION 
 * @author mreri
 *
 */
@Configuration
@EnableSwagger2
@ComponentScan(basePackages = "com.eric.erwin.BudgetWeApp")
public class SwaggerConfig {

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("budget-api").select().paths(paths()).build().apiInfo(apiInfo());
	}
	
	@SuppressWarnings("unchecked")
	private Predicate<String> paths() {
		return or(regex("/budget.*"));
	}
	
	@SuppressWarnings("deprecation")
	private ApiInfo apiInfo() {
		return new ApiInfo("Budget", "Budget Services", "1.0", "Simple Budget Service Program", "Eric Erwin", "Budget Services Application", "Simple Budget Service Application");
	}
}
